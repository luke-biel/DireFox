# EngiBrew

language Nim  

To implement:  
math library: em -> https://github.com/SteamPoweredAnimal/em  
windows: x11 and winAPI  
input: x11 and winAPI  
sound: openAL  
graphics backend: openGL and Vulkan (later on)  
mobiles: libphone? -> https://github.com/huxingyi/libphone  
networking: nim built-in  
scripting: nim should be sufficient, but I should be able to enable js scripting. Dunno if I should add anything else, like lua.  
